package example;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    double position = 0;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLACK);
        g.fillRect((int)position, 20, 10, 10);
    }

    public void update() {
        position = position + 0.0001;
    }
}
