package example;

import javax.swing.*;
import java.awt.*;

public class Move {

    public static void main(String[] args) {
        JFrame frame = new JFrame("This is a title");
        Panel p = new Panel();
        frame.add(p, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1080, 720);
        frame.setVisible(true);

        while (true) {
            p.update();
            p.repaint();
        }


    }

}
