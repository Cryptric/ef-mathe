package aufgaben1.solution;

public class Homework {

    public static void main(String[] args) {

        System.out.println("First homework");


        // TODO Task 1: Write some code to print out the first 50 elements of the sequence {3, 7, 11, 15, 19, ...}, use a for loop
        int x = 3;
        System.out.println(x);
        for(int i = 1; i < 50; i = i + 1) {
            x = x + 4;
            System.out.println(x);
        }


        // TODO Task 2: Write a function to calculate the area of a circle with radius r
        // Call the function, set 4.6 for r and print out the result
        System.out.println(circleArea(4.6));


        // TODO Task 3: Write a function to calculate the volume of a cylinder with radius r and height h
        // Call the function, set 2.4 for r, 5.4 for h and print out the result
        System.out.println(cylinderVolume(2.4, 5.4));
    }


    public static double circleArea(double r) {
        double a = (r * r) * Math.PI;
        return a;
    }

    public static double cylinderVolume(double r, double h) {
        double v = r * r * Math.PI * h;
        return v;
    }

}


