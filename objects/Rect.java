package objects;

public class Rect {

    double a;
    double b;

    public Rect(double x, double y) {
        a = x;
        b = y;
    }

    public double area() {
        double area = a * b;
        return area;
    }

    public double circumference() {
        double circumference = 2 * (a + b);
        return circumference;
    }
}
