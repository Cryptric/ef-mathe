package objects;

public class Test {

    public static void main(String[] args) {
        Rect r1 = new Rect(2.5, 5);

        System.out.println(r1.area());
        System.out.println(r1.circumference());
    }
}
