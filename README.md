# Setup
In order to get more serious we need real tools!

## Install Java Development Kit (JDK)

Already installed?

```javac -version```

https://www.oracle.com/java/technologies/javase-jdk15-downloads.html


## Install Integrated development environment (IDE)

https://www.jetbrains.com/idea/download/

# Java Cheat-Sheet

https://cryptpad.fr/pad/#/2/pad/edit/Tyc4NQWTaPfVPEpyDfGMU0HV/
