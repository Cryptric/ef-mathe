package pack;

public class AreaCalculations {

    public static void main(String[] args) {
        double areaCircle = circleArea(5);
        System.out.println("Circle area: " + areaCircle);

        double areaRect = rectangle(3, 7);
        System.out.println("Rect area: " + areaRect);
    }

    public static double circleArea(double r) {
        double area =  Math.PI * r * r;
        return area;
    }

    public static double rectangle(double a, double b) {
        double area = a * b;
        return area;
    }

}
