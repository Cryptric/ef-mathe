package pack;

public class E {

    public static void main(String[] args) {

        double e = 1;
        for (int i = 1; i < 20; i = i + 1) {
            e = e + 1.0 / faculty(i);
        }

        System.out.println("Calculated e: " + e);
        System.out.println("Actual e: " + Math.exp(1));
        System.out.println("Diff: " + (Math.exp(1) - e));
    }

    public static long faculty(int n) {
        long result = 1;
        for (int i = 1; i <= n; i = i + 1) {
            result = result * i;
        }
        return result;
    }


}
