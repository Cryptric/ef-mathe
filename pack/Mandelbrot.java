package pack;

public class Mandelbrot {

    public static void main(String[] args) {

        double[] c1 = new double[2]; // Create new variable list with a length of 2
        c1[0] = 4.5; // We use the first list element to store the real part
        c1[1] = 5.7; // We use the second list element to store the complex part

        double[] c2 = new double[2];
        c2[0] = 3.8;
        c2[1] = 8.3;

        double[] c3 = new double[2];

        c3 = add(c1, c2);

        System.out.println(c3[0]);
        System.out.println(c3[1]);

    }

    public static double[] square(double[] complexNumber) {
        double[] result = new double[2];

        // TODO write code to square the "complexNumber" and store the result in "result"

        return result;
    }

    public static double[] add(double[] complexNumber1, double[] complexNumber2) {
        double[] result = new double[2];
        result[0] = complexNumber1[0] + complexNumber2[0];
        result[1] = complexNumber1[1] + complexNumber2[1];
        return result;
    }
    
}
